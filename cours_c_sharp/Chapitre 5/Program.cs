﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Library biblio = new Library("Chez Charley");
            biblio.AddBook(new Book("Harry Potter", "JK Rowling", "Grimard", "2000", "12345"));
            biblio.AddBook(new Book("La Ligne Verte", "Stephen King", "Inconnu", "1996", "122"));
            biblio.AddBook(new Book("La Ligne Jaune", "Stephen King", "Inconnu", "1996", "122"));
            biblio.AddBook(new Book("La Ligne Rose", "Stephen King", "Inconnu", "1996", "122"));
            biblio.AddBook(new Book("Harry Potter 2", "JK Rowling", "Grimard", "2001", "12345"));
            biblio.DeleteBook(biblio.SearchBooksByAuthor("Stephen King"));
            biblio.PrintBooks();

            Employee emp = new Employee("Charles", "Viau", 123, "03/10/93", 123456789);
            emp.Display();
        }
    }

    class Student
    {
        private string Nom { get; set; }
        private string Prenom { get; set; }
        private string Cours { get; set; }
        private string Sujet { get; set; }
        private string Universite { get; set; }
        private string Email { get; set; }
        private string Telephone { get; set; }

        public static int NbInstance = 0;

        Student(string nom, string prenom, string universite, string telephone)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Universite = universite;
            this.Telephone = telephone;
            Student.NbInstance++;
        }

        Student(string nom, string prenom, string cours, string sujet,
            string universite, string email, string telephone)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Universite = universite;
            this.Telephone = telephone;
            this.Cours = cours;
            this.Sujet = sujet;
            this.Email = email;
            Student.NbInstance++;
        }

        ~Student()
        {
            Student.NbInstance--;
        }

        public void Print()
        {
            Console.WriteLine($"{this.Prenom}, {this.Nom}, {this.Universite}, {this.Cours}, " +
                $"{this.Sujet}, {this.Email}, {this.Telephone}");
        }

        public static Student Create()
        {
            Console.WriteLine("Input Last Name");
            string name = Console.ReadLine();
            Console.WriteLine("Input First name");
            string fname = Console.ReadLine();
            Console.WriteLine("Input University");
            string university = Console.ReadLine();
            Console.WriteLine("Input phone number");
            string phone = Console.ReadLine();

            return new Student(name, fname, university, phone);
        }
    }

    class Library
    {
        public string Name { get; set; }
        public List<Book> BookList = new List<Book>();

        public Library(string name)
        {
            this.Name = name;
        }

        public void PrintBooks()
        {
            foreach (var book in this.BookList)
            {
                book.Print();
            }
        }

        public void AddBook(Book book)
        {
            this.BookList.Add(book);
        }

        public List<Book> SearchBooksByAuthor(string author)
        {
            List<Book> searchResultList = new List<Book>();

            foreach (var Books in this.BookList)
            {
                if (Books.Author == author)
                {
                    searchResultList.Add(Books);
                }
            }

            return searchResultList;
        }

        public void DeleteBook(Book book)
        {
            this.BookList.Remove(book);
        }

        public void DeleteBook(List<Book> booksToDelete)
        {
            foreach (var book in booksToDelete)
            {
                for (int i = 0; i < this.BookList.Count; i++)
                {
                    if (book == this.BookList[i])
                    {
                        this.BookList.Remove(this.BookList[i]);
                    }
                }
            }
        }
    }

    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Editor { get; set; }
        public string DateEdition { get; set; }
        public string ISBN { get; set; }

        public Book(string title, string author, string editor, string dateEdition, string isbn)
        {
            this.Title = title;
            this.Author = author;
            this.Editor = editor;
            this.DateEdition = dateEdition;
            this.ISBN = isbn;
        }

        public static Book create()
        {
            Console.WriteLine("Input Title");
            string title = Console.ReadLine();
            Console.WriteLine("Input Author");
            string author = Console.ReadLine();
            Console.WriteLine("Input Editor");
            string editor = Console.ReadLine();
            Console.WriteLine("Input date of edition");
            string dateEdition = Console.ReadLine();
            Console.WriteLine("Input ISBN");
            string isbn = Console.ReadLine();

            return new Book(title, author, editor, dateEdition, isbn);
        }

        public void Print()
        {
            Console.WriteLine("Book Info : \n" +
                $"Title : {this.Title}\n" +
                $"Author : {this.Author}\n" +
                $"Editor : {this.Editor}\n" +
                $"Date Edition : {this.DateEdition}\n" +
                $"ISBN : {this.ISBN}\n" + "\n");
        }
    }

    partial class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public string Birthday { get; set; }
        public int SocialNumber { get; set; }

    }

    partial class Employee
    {
        public Employee(string fName, string lName, int id, string BDay, int socialNumber)
        {
            this.FirstName = fName;
            this.LastName = lName;
            this.Id = id;
            this.Birthday = BDay;
            this.SocialNumber = socialNumber;
        }

        public void Display()
        {
            Console.WriteLine($"First Name { this.FirstName }\n" +
                $"Last Name : {this.LastName}\n"+
                $"ID : {this.Id}\n" +
                $"Birthday : {this.Birthday}\n" +
                $"Social Number : {this.SocialNumber}\n");
        }
    }
}
