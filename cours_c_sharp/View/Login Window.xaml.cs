﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using BLL;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if(AdminManager.CheckLogin(new Admin(this.UsernameTextBox.Text, this.PasswordTextBox.Password.ToString())))
            {
                MessageBox.Show(this, "Succesfully Login.", "Succes Message", MessageBoxButton.OK);
                new AddStudentWindow(this.UsernameTextBox.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Username or Password isn't valid.", "Error Message", MessageBoxButton.OK);
            }
        }
    }
}
