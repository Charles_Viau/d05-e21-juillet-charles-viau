﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;
using BLL;

namespace View
{
    /// <summary>
    /// Interaction logic for AddStudentWindow.xaml
    /// </summary>
    public partial class AddStudentWindow : Window
    {
        public AddStudentWindow(string adminName)
        {
            InitializeComponent();
            this.AdminLabel.Content = $"Admin : {adminName}";
            this.Show();
        }

        private static bool IsNameValid(string name)
        {
            return Regex.Match(name, @"^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$").Success;
        }

        private static bool IsAgeValid(string number)
        {
            return Regex.Match(number, @"^([0-9]|[0-9][0-9])$").Success;
        }

        private static bool? ConvertCheckBox(CheckBox box)
        {
            return box.IsChecked == null ? null : box.IsChecked;
        }

        private bool CheckNameConstraint(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                this.OperationLabel.Content = "Insertion error, Name field is empty.";
                return false;
            }
            if (StudentManager.isNameTaken(name))
            {
                this.OperationLabel.Content = "Insertion error, Name is already taken.";
                return false;
            }
            if(!IsNameValid(name))
            {
                this.OperationLabel.Content = "Insertion error, Name format isn't valid.\n" + "Name must start with a capital letter and must not contain number";
            }

            return true;
        }

        private bool CheckAgeConstraint(string age)
        {
            if(String.IsNullOrEmpty(age))
            {
                this.OperationLabel.Content = "Insertion error, Age field is empty.";
            }
            if(!IsAgeValid(age))
            {
                this.OperationLabel.Content = "Insertion error, Age format isn't valid.";
                return false;
            }
            
            if(int.Parse(age) < Student.minimalAge)
            {
                this.OperationLabel.Content = $"Insertion error, Age must be above {Student.minimalAge}.";
                return false;
            }

            return true;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if(CheckNameConstraint(this.NameTextBox.Text) && CheckAgeConstraint(this.AgeTextBox.Text))
            {
                Student student = new Student(this.NameTextBox.Text, int.Parse(this.AgeTextBox.Text), 
                    this.BirthdayPicker.SelectedDate, ConvertCheckBox(this.IsInternationCheckBox));

                long id = StudentManager.Add(student);

                this.OperationLabel.Content = $"Student with id : {id} sucessfully added to database.";
            }
        }
    }


}
