﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_2
{
    class Program
    {
        static void Main(string[] args)
        {
            TypeEtVariable.TypeVariable();
            InstructionDeControleDFlot.InstructionControle();

        }
    }

    class TypeEtVariable
    {
        public static void TypeVariable()
        {
            Console.WriteLine("1. Déclarer quelques variables en choisissant, pour chacune d'elles, le type le plus approprié\n" +
                "52.130 : float\n" +
                "-115 : sbyte\n" +
                "4 825 932 : int\n" +
                "97 : byte\n" +
                "-10 000 : short\n" +
                "20 000 : short\n" +
                "224 : byte\n" +
                "970 700 000 : int\n" +
                "112 : sbyte\n" +
                "-44 : sbyte\n" +
                "-1 000 000 : int\n" +
                " 1990 : short\n" +
                "123 456 789 123 456 789 : long\n");

            //#2
            //bool isMale = true;

            /*#3 Déclarer deux variables de type string avec les valeurs "Hello" et "World". 
             Déclarer une variable de type object. Assigner la valeur obtenue par la concaténation des deux chaînes 
            (ajouter des espaces si nécessaire) à cette variable. Afficher la variable de type object.*/

            string hello = "Hello";
            string world = "World";
            object helloWorld = hello + " " + world;
            Console.WriteLine(helloWorld);
        }
    }

    class InstructionDeControleDFlot
    {
        public static void InstructionControle()
        {
            Console.WriteLine("Entrez une note (0 a 9)");
            int note = Convert.ToInt32(Console.ReadLine());

            switch (note)
            {
                case 7:
                case 8:
                case 9:
                    note = note * 1000;
                    break;
                case 4:
                case 5:
                case 6:
                    note = note * 100;
                    break;
                case 1:
                case 2:
                case 3:
                    note = note * 10;
                    break;
                default:
                    {
                        Console.WriteLine("Erreur");
                    }
                    break;
            }

            Console.WriteLine(note);
        }
    }

    class Enumeration
    {
        enum ChatStatus
        {
            ONLINE,
            OFFLINE,
            BUSY
        }

        enum Gender
        {
            MAN,
            WOMAN
        }

        class Chat
        {
            string username;
            Gender gender;
            byte userage;
            string email;
            ChatStatus status;
        }
    }

    class tableau
    {

    }


}

