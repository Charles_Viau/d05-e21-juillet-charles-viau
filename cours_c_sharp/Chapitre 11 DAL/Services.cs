﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapitre_11_Model;

namespace Chapitre_11_DAL
{
    public static class Services
    {
        public static void HandleNull(string param, object obj, SqlCommand cmd)
        {
            if (obj == null)
            {
                cmd.Parameters.AddWithValue(param, DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue(param, obj);
            }
        }


        public static void MakeRequest(Request request, Student student)
        {
            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=MSI\SQLEXPRESS;Initial Catalog=MTest;Integrated Security=True;Connect Timeout=5"))
            {
                sqlConnection.Open();

                switch (request)
                {
                    case Request.ADD_STUDENT:
                        AddStudent(sqlConnection, student);
                        break;
                    default:
                        break;
                }
            };
        }

        private static void AddStudent(SqlConnection conn, Student stu)
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Student(FirstName, LastName, isInternational, BirthDate, YearsOfExperience, ID)" +
                    "VALUES (@FirstName, @LastName, @IsInternational, @BirthDate, @YearsOfExperience, @Id)";

                cmd.Parameters.AddWithValue("FirstName", stu.FirstName);
                cmd.Parameters.AddWithValue("LastName", stu.LastName);
                cmd.Parameters.AddWithValue("IsInternational", stu.IsInternationalStu);
                cmd.Parameters.AddWithValue("Id", stu.Id);
                HandleNull("BirthDate", stu.DOB, cmd);
                HandleNull("YearsOfExperience", stu.YearsOfExperience, cmd);

                cmd.ExecuteNonQuery();
            }
        }



        private static void RemoveStudent(SqlConnection conn, Student stu)
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
             
                

                cmd.ExecuteNonQuery();
            }
        }


    }

    public class AdminService
    {
        public bool ChekLogin(Admin admin)
        {
            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=MSI\SQLEXPRESS;Initial Catalog=MTest;Integrated Security=True;Connect Timeout=5"))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = @"SELECT COUNT (*) FROM T_Admin WHERE [name] = @Name and [password]=@Password";
                    cmd.Parameters.Add(new SqlParameter("Name", admin.Name));
                    cmd.Parameters.Add(new SqlParameter("Password", admin.Password));
                    return (int)cmd.ExecuteScalar() == 1 ? true : false;
                    /*using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return true;
                        }
                    }*/
                }
            }
            //return false;
        }
    }
}

