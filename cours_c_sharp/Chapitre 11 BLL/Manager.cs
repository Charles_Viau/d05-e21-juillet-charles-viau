﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapitre_11_Model;
using Chapitre_11_DAL;

namespace Chapitre_11_BLL
{
    public class Manager
    {
        public static void AddStudent(Student student)
        {
            Services.MakeRequest(Request.ADD_STUDENT, student);
        }
    }
}
