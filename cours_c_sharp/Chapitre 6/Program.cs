﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animalList = new List<Animal>();
            animalList.Add(new Cat(10, "toto", Sexe.MALE));
            animalList.Add(new Dog(12, "tata", Sexe.MALE));
            animalList.Add(new Frog(2, "titi", Sexe.FEMALE));

            foreach (var animal in animalList)
            {
                animal.MakeNoise();
            }

            Person p1 = new Person();
            KitchenAppliance microwave = new Microwave("microW", new DateTime(1992, 10, 03));
            KitchenAppliance stove = new Stove("Stovus", new DateTime(1993, 09, 21));
            p1.useKitcheAppliance(microwave);
            p1.useKitcheAppliance(stove);

            Employee emp1 = new EmployeeA("Francis", "Montreal", 50000);
            emp1.GetMonthlySalary();

            StorageDevice phone = new SmartPhone();
            StorageDevice usb = new USBDisk();
            Computer cpu = new Computer();
            cpu.ReadData(phone);
            cpu.WriteData(usb);

            ProgramHelper prg = new ProgramHelper();
            prg.ConvertToCSharp("allo");
            prg.ConvertToVB("allo");
        }
    }

    class GroupeEtudiant
    {
        public string GroupId { get; set; }
        List<Prof> profList = new List<Prof>();
        List<Etudiant> etudiantList = new List<Etudiant>();
    }

    class Prof : Personne
    {
        List<Cours> coursList = new List<Cours>();

        Prof(string nom) : base(nom)
        {

        }
    }

    class Cours
    {
        public string Nom { get; set; }
        public int NbExercices { get; set; }
        public int NbGroupe { get; set; }
    }

    class Etudiant : Personne
    {
        public int Id { get; set; }

        public Etudiant(string nom, int id) : base(nom)
        {
            this.Id = id;

        }
    }

    abstract class Personne
    {
        public string Nom { get; set; }

        protected Personne(string nom)
        {
            this.Nom = nom;
        }
    }

    abstract class Human
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        protected Human(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }

    class Student : Human
    {
        int Mark { get; set; }

        Student(string firstName, string lastName, int mark) : base(firstName, lastName)
        {
            this.Mark = mark;
        }
    }

    class Worker : Human
    {
        public double Rate { get; set; }
        public double HoursWorked { get; set; }

        Worker(string firstName, string lastName) : base(firstName, lastName)
        {
            this.Rate = 14.75;
            this.HoursWorked = 0;
        }

        public double CalculateSalary()
        {
            return this.HoursWorked * this.Rate;
        }

    }

    public enum Sexe
    {
        MALE,
        FEMALE
    }

    abstract class Animal
    {
        public int Age { get; set; }
        public string Nom { get; set; }
        public Sexe Sex { get; set; }

        protected Animal(int age, string nom, Sexe sexe)
        {
            this.Age = age;
            this.Nom = nom;
            this.Sex = sexe;
        }

        virtual public void MakeNoise()
        {
            Console.WriteLine(this.Nom + " does ");
        }
    }

    class Dog : Animal
    {
        public Dog(int age, string nom, Sexe sexe) : base(age, nom, sexe) { }

        public override void MakeNoise()
        {
            base.MakeNoise();
            Console.WriteLine("Woof");
        }
    }

    class Frog : Animal
    {
        public Frog(int age, string nom, Sexe sexe) : base(age, nom, sexe) { }

        public override void MakeNoise()
        {
            base.MakeNoise();
            Console.WriteLine("Croak");
        }
    }

    class Cat : Animal
    {
        public Cat(int age, string nom, Sexe sexe) : base(age, nom, sexe) { }

        public override void MakeNoise()
        {
            base.MakeNoise();
            Console.WriteLine("Miaou");
        }
    }

    abstract class KitchenAppliance
    {
        public string Brand { get; set; }
        public DateTime ManifacturingDate { get; set; }

        protected KitchenAppliance(string brand, DateTime date)
        {
            this.Brand = brand;
            this.ManifacturingDate = date;
        }

        virtual public void PowerOn()
        {

        }
    }

    class Stove : KitchenAppliance
    {
        public Stove(string brand, DateTime date) : base(brand, date) { }

        override public void PowerOn()
        {
            Console.WriteLine("Stove has overheated");
        }
    }

    class Microwave : KitchenAppliance
    {
        public Microwave(string brand, DateTime date) : base(brand, date) { }

        override public void PowerOn()
        {
            Console.WriteLine("microwave has exploded");
        }
    }

    class Person
    {
        public void useKitcheAppliance(KitchenAppliance appliance)
        {
            appliance.PowerOn();
        }
    }

    abstract class Employee
    {
        public string Nom { get; set; }
        public string Adresse { get; set; }
        public double SalaryYear { get; set; }

        protected Employee(string nom, string adresse, double salary)
        {
            this.Nom = nom;
            this.Adresse = adresse;
            this.SalaryYear = salary;
        }

        abstract public double GetMonthlySalary();
    }

    class EmployeeA : Employee
    {
        public EmployeeA(string nom, string adresse, double salary) : base(nom, adresse, salary) { }

        override public double GetMonthlySalary()
        {
            return this.SalaryYear / 12;
        }
    }

    abstract class StorageDevice
    {
        abstract public void Read();
        abstract public void Wrtite();

    }
    class USBDisk : StorageDevice
    {
        public override void Read()
        {
            Console.WriteLine("la cle usb lit l'info");
        }

        public override void Wrtite()
        {
            Console.WriteLine("la cle usb ecrit l'info");
        }
    }

    class SmartPhone : StorageDevice
    {
        public override void Read()
        {
            Console.WriteLine("Le smart phone lis");
        }

        public override void Wrtite()
        {
            Console.WriteLine("le smart phone ecrit");
        }
    }
    class Computer
    {
        public void ReadData(StorageDevice device)
        {
            Console.WriteLine("l'ordinateur lit : " + device.GetType());
        }

        public void WriteData(StorageDevice device)
        {
            Console.WriteLine("l'ordinateur ecrit sur " + device.GetType());
        }
    }

    interface IConvertible
    {
        string ConvertToCSharp(string codeToConvert);
        string ConvertToVB(string codeToConvert);
    }

    class ProgramHelper : ICodeChecker
    {
        public string ConvertToCSharp(string codeToConvert)
        {
            Console.WriteLine("Code converti vers c sharp");
            return codeToConvert;
        }

        public string ConvertToVB(string codeToConvert)
        {
            Console.WriteLine("code converti vers vb");
            return codeToConvert;
        }

        bool ICodeChecker.CodeCheckSyntax(string chaine1, string chaine2)
        {
            Console.WriteLine("code has been checked");
            return true;
        }
    }

    interface ICodeChecker : IConvertible
    {
        bool CodeCheckSyntax(string chaine1, string chaine2);
    }
}
