﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_11_Model
{
    public class Student
    {
        public Guid Id { get;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsInternationalStu { get; set; }
        public int? YearsOfExperience { get; set; }
        public DateTime? DOB { get; set; }

        public Student(string firstName, string lastName, bool isInternational, int? experience, DateTime? birthdate)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.IsInternationalStu = isInternational;
            this.YearsOfExperience = experience;
            this.DOB = birthdate;
            this.Id = Guid.NewGuid();
        }

        public void Print()
        {
            Console.WriteLine("Id:" + this.Id);
            Console.WriteLine("Name:" + this.FirstName + " " + this.LastName);
            Console.WriteLine("IsInternationalStu:" + this.IsInternationalStu);
            Console.WriteLine("YearsOfExperience:" + this.YearsOfExperience);
            Console.WriteLine("DOB:" + this.DOB);
            Console.WriteLine("---------------------------------------");
        }
    }
}
