﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_11_Model
{
    public class Admin
    {
        public Guid Id { get; } 
        public string Name { get; private set; }
        public string Password {get; private set; }

        public Admin(string name, string password)
        {
            this.Name = name;
            this.Password = password;
            this.Id = Guid.NewGuid();
        }
    }
}
