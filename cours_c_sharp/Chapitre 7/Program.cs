﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_7
{
    public delegate void DGSaySomething(string chaine);

    public class Program
    {
        static void Main(string[] args)
        {
            Chinese chintok = new Chinese();
            DGSaySomething dg = chintok.SaySomething;
            dg?.Invoke("ni hao");

            Door door = new Door();
            Owner owner = new Owner();
            Guard guard = new Guard();

            door.OnOpenEvent += owner.onDoorOpen;
            door.IsOpen = true;

            Person receptionist = new SchoolReceptionist("GRETTA");
            Person leDirecteur = new Director((SchoolReceptionist)receptionist);
            Person daTeacher = new Teacher("THUNBERG");
            ((Teacher)daTeacher).CallSick((SchoolReceptionist)receptionist, 10);
            
        }
    }

    public abstract class Person
    {
        public delegate void DGSaySomething(string chaine);

        virtual public void SaySomething(string chaine)
        {
            Console.WriteLine(chaine);
        }

        virtual public void NotifyTeacherIsSick(Teacher teacher, int days)
        {
            Console.WriteLine($"Damn! {teacher.Name} is going to be sick for {days} days!");
        }
    }

    public class Chinese : Person
    {
        public override void SaySomething(string chaine)
        {
            Console.WriteLine(chaine);
        }
    }

    public class Door
    {
        public int ID { get; set; }
        private bool isOpen;
        public bool IsOpen
        {
            get
            {
                return isOpen;
            }
            set
            {
                this.isOpen = value;
                if(this.isOpen)
                {
                    OnOpenEvent?.Invoke(this, new DoorOpenEventArgs(DateTime.Now));
                }
            }
        }

        public event EventHandler<DoorOpenEventArgs> OnOpenEvent;
    }

    public class DoorOpenEventArgs : EventArgs
    {
  
        public DateTime EventTime { get;}

        public DoorOpenEventArgs(DateTime time)
        {
            this.EventTime = time;
        }
    }

    public class Owner
    {
        public Owner()
        {

        }

        public void onDoorOpen(object door, DoorOpenEventArgs eventArgs)
        {
            Console.WriteLine("The door is open!");
        }
    }

    public class Guard
    {

    }

    public class SchoolReceptionist : Person
    {
        public string Name { get; }
        public List<Person> ListToNotifyOnTeacherCallSickEvent = new List<Person>();
        public Action<string, int> TeacherSick;

        public SchoolReceptionist(string name)
        {
            this.Name = name;
        }

        public void NotifyPeople(Teacher woman, int days)
        {
            foreach (var Person in this.ListToNotifyOnTeacherCallSickEvent)
            {
                Person.NotifyTeacherIsSick(woman, days);
            }
        }

    }

    public class Teacher : Person
    {
        public string Name { get; }

        public Teacher(string name)
        {
            this.Name = name;
        }

        public void CallSick(SchoolReceptionist woman, int days)
        {
            woman.NotifyPeople(this, days );
        }
    }

    public class Director : Person
    {
        public Director(SchoolReceptionist receptionist)
        {
            receptionist.ListToNotifyOnTeacherCallSickEvent.Add(this);
        }
    }

}
