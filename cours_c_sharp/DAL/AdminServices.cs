﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL
{
    public static class AdminServices
    {
        public static bool ChekLogin(Admin admin)
        {
            using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=MSI\SQLEXPRESS;Initial Catalog=DBExamenFinalCSharp;Integrated Security=True;Connect Timeout=5"))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = @"SELECT COUNT (*) FROM T_Admin WHERE [name] = @Username and [password]=@Password";
                    cmd.Parameters.AddWithValue("Username", admin.Username);
                    cmd.Parameters.AddWithValue("Password", admin.Password);
                    return (int)cmd.ExecuteScalar() == 1 ? true : false;
                }
            }

        }
    }
}


