﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL
{
    public static class StudentServices
    {
        public static long Add(Student stu)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=MSI\SQLEXPRESS;Initial Catalog=DBExamenFinalCSharp;Integrated Security=True;Connect Timeout=5"))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO T_Student(name, isInternationalStudent, birthday, age) output inserted.id VALUES (@Name, @IsInternational, @Birthday, @Age)";

                    cmd.Parameters.AddWithValue("Name", stu.Name);
                    cmd.Parameters.AddWithValue("Age", stu.Age);
                    DBUtility.HandleNull("Birthday", stu.Birthday, cmd);
                    DBUtility.HandleNull("IsInternational", stu.IsInternational, cmd);

                    return (long)cmd.ExecuteScalar();
                }
            }
        }

        public static bool isNameTaken(string name)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=MSI\SQLEXPRESS;Initial Catalog=DBExamenFinalCSharp;Integrated Security=True;Connect Timeout=5"))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) FROM T_Student WHERE name = @Name";
                    cmd.Parameters.AddWithValue("Name", name);
                    cmd.ExecuteNonQuery();
                    return (int)cmd.ExecuteScalar() == 1 ? true : false;
                }
            }
        }
    }
}

