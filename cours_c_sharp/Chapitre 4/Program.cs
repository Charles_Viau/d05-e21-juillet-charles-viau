﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = { 1, 2, 3, 4, 9, 5, 6, 7, 8, 0 };
            float[] data = { 1.5f, 0.7f, 8.0f };

        }
    }

    class numero1
    {
        //Créer une méthode GetMax() avec deux entier en paramètres(int) qui retourne le maximum de deux nombres.
        //Écrire un programme qui lit trois nombres dans la console et affiche le plus grand des trois.Utiliser la méthode GetMax() précédemment créée.
        //Écrire un programme de test qui valide que la méthode fonctionne correctement.

        public static int GetMax(int a, int b)
        {
            return a > b ? a : b;
        }

        public static int Programme()
        {
            Console.WriteLine("Input 3 values, get the biggest one.(Press Enter between each value)");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());
            return GetMax(GetMax(a, b), c);
        }

        public static int ProgrammeTest()
        {
            return GetMax(GetMax(1, 2), 3);
        }
    }

    class numero2
    {
        //Écrire une méthode qui retourne le nom français du dernier chiffre d'un nombre donné. Exemple : pour 512 afficher "deux"; pour 1024 afficher "quatre".

        public static void PrintLastNumber()
        {
            Console.WriteLine("Enter a number.");
            string number = Console.ReadLine();
            int lastNumber = int.Parse(number.Substring(number.Length - 1, 1));
            switch (lastNumber)
            {
                case 1:
                    {
                        Console.WriteLine("un");
                    }
                    break;
                case 2:
                    {
                        Console.WriteLine("deux");
                    }
                    break;
                case 3:
                    {
                        Console.WriteLine("trois");
                    }
                    break;
                case 4:
                    {
                        Console.WriteLine("quatre");
                    }
                    break;
                case 5:
                    {
                        Console.WriteLine("5");
                    }
                    break;
                case 6:
                    {
                        Console.WriteLine("six");
                    }
                    break;
                case 7:
                    {
                        Console.WriteLine("sept");
                    }
                    break;
                case 8:
                    {
                        Console.WriteLine("huit");
                    }
                    break;
                case 9:
                    {
                        Console.WriteLine("neuf");
                    }
                    break;
                case 0:
                    {
                        Console.WriteLine("zero");
                    }
                    break;
            }
        }
    }

    class numero3
    {
        //Écrire une méthode qui trouve combien de fois un certain nombre peut être trouvé dans un tableau donné. 
        //Écrire un programme pour tester que la méthode fonctionne correctement.

        public static int FindTimeOfNumber(int[] tab, int valeur)
        {
            int count = 0;
            foreach (int elem in tab)
            {
                if (elem == valeur)
                {
                    count++;
                }
            }

            return count;
        }
    }

    class numero4
    {
        //Écrire une méthode qui teste si un élément, à une certaine position dans un tableau est plus grand que ses deux voisins. Tester si la méthode fonctionne correctement.\

        public static bool TestPlusGrandQueVoisin(int[] tab, int position)
        {
            try
            {
                if (position < 0 || position > tab.Length - 1)
                {
                    throw new ArgumentException("position must be between 0 and " + (tab.Length - 1));
                }
                if (position == 0)
                {
                    return (numero1.GetMax(tab[0], tab[1]) == tab[0]) ? true : false;
                }
                if (position == tab.Length - 1)
                {
                    return (numero1.GetMax(tab[tab.Length - 1], tab[tab.Length - 2]) == tab[tab.Length - 1]) ? true : false;
                }
                else
                {
                    return (numero1.GetMax(numero1.GetMax(tab[position], tab[position - 1]), tab[position + 1]) == tab[position]) ? true : false;
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("position must be between 0 and " + (tab.Length - 1) + " Input a new position.");
                int position2 = int.Parse(Console.ReadLine());
                return TestPlusGrandQueVoisin(tab, position2);
            }
        }
    }

    class numero5
    {
        //Écrire une méthode qui retourne l'indice du plus grand élément d'un tableau. Utiliser cette méthode pour implémenter un tri par ordre croissant.

        public static int TriEtRetournePlusGrand(int[] tab)
        {
            Array.Sort(tab);
            return tab.Length - 1;
        }
    }

    class numero6
    {
        //Écrire une fonction nommée MinMaxArray qui retourne les valeurs minimale et le maximale stockées dans un tableau, en utilisant des paramètres par référence :

        public static void MinMaxArray(float[] tab, out float minimum, out float maximum)
        {
            Array.Sort(tab);
            minimum = tab[0];
            maximum = tab[tab.Length - 1];
        }

        //(après l'appel, minimum contiendra 0.7, et maximum contiendra 8.0)
    }

    class numero7
    {
        //Écrire une méthode nommée "Double" pour calculer le double d'un nombre entier, et modifier la donnée passée en argument. 
        //Cela doit être une fonction void et il faut utiliser le modificateur de paramètre out.

        public static void Double(int val, out int number)
        {
            number = val * 2;
        }
    }

    class numero8
    {
        //Écrire une méthode nommée "Add" qui recevra un nombre quelconque d'entiers en paramètre, à l'exécution, et qui retourne la somme de tous ces nombres.
        //On doit utliser la modificateur params.

        public static int Add(params int[] value)
        {
            int somme = 0;
            for (int i = 0; i < value.Length; i++)
            {
                somme = somme + value[i];
            }

            return somme;
        }
    }
}