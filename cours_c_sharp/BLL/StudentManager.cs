﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DAL;

namespace BLL
{
    public static class StudentManager
    {
        public static long Add(Student student)
        {
            return StudentServices.Add(student);
        }

        public static bool isNameTaken(string name)
        {
            return StudentServices.isNameTaken(name);
        }
    }
}
