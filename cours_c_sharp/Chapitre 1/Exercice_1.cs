﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Exercice_1.Exercice1();
            Exercice_2.Exercice2();
            Exercice_3.Exercice3();
        }

    }

    class Exercice_1
    {
        static public void Exercice1()
        {
            double pi = 3.14159265358979;

            Console.WriteLine("Saisir le rayon du cercle.\n");
            double rayon = Convert.ToDouble(Console.ReadLine());
            double perimetre = 2 * pi * rayon;
            double aire = pi * Math.Pow(rayon, 2);
            Console.WriteLine("Le perimetre est de " + perimetre + " et l'aire est de " + aire + "\n");
        }
    }

    class Exercice_2
    {
        static public void Exercice2()
        {
            Gerant gerant = Gerant.CreateGerant();
            Compagnie maxi = Compagnie.CreateCompagnie(gerant);
            maxi.Print();
        }

        class Compagnie
        {
            string nom;
            string adresse;
            string telephone;
            string numeroFax;
            string siteWeb;
            Gerant gerant;

            Compagnie(string NOM, string ADRESSE, string TELEPHONE, string NUMERO_FAX, string SITE_WEB, Gerant GERANT)
            {
                this.nom = NOM;
                this.adresse = ADRESSE;
                this.telephone = TELEPHONE;
                this.numeroFax = NUMERO_FAX;
                this.siteWeb = SITE_WEB;
                this.gerant = GERANT;
            }

            public void Print()
            {
                Console.WriteLine("Information de la compagnie\n"
                    + this.nom + "\n"
                    + this.adresse + "\n"
                    + this.telephone + "\n"
                    + this.numeroFax + "\n"
                    + this.siteWeb + "\n");
                this.gerant.Print();
            }

            public static Compagnie CreateCompagnie(Gerant gerant)
            {
                return new Compagnie("Maxi", "23 rue Montreal", "1-514-888-8888", "4567", "www.maxi.com", gerant);
            }

        }

        class Gerant
        {
            string nom;
            string prenom;
            string telephone;

            public Gerant(string NOM, string PRENOM, string TELEPHONE)
            {
                this.nom = NOM;
                this.prenom = PRENOM;
                this.telephone = TELEPHONE;
            }

            public string GetNom()
            {
                return this.nom;
            }

            public void Print()
            {
                Console.WriteLine("Information du gerant\n"
                    + this.nom + "\n"
                    + this.prenom + "\n"
                    + this.telephone + "\n");
            }

            public static Gerant CreateGerant()
            {
                return new Gerant("Lafouine", "Gerard", "1-514-888-8888");
            }
        }
    }

    class Exercice_3
    {
        public static void Exercice3()
        {
            Console.WriteLine("Entrez autant de nombre entier que vous le voulez, puis entrez 0 pour obtenir la somme!\n");
            bool exit = false;
            int somme = 0;
            int nombre;
            while (exit == false)
            {
                nombre = Convert.ToInt32(Console.ReadLine());
                if (nombre == 0)
                {
                    exit = true;
                }
                somme = somme + nombre;
            }
            Console.WriteLine("La somme est de : " + somme);
        }


    }
}
