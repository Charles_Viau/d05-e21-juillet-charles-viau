﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapitre_8
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>();
            personList.Add(new Student("Charles", 27));
            personList.Add(new Employee("Marc-Antoine", 25, 70000));
            personList.ForEach(Console.WriteLine);

            Mathematics.GetPrimes(0, 99);


            /*foreach (var persons in personList)
            {
                // Could use "is" keyword, used pattern matching instead
                  switch (persons)
                {
                    case Student student:
                        Console.WriteLine($"{student.Name},{student.Age}")
                        break;
                    case Employee employee:
                        Console.WriteLine($"{employee.Name}, {employee.Salary}");
                        break;
                
                Console.WriteLine(persons);
            }*/
        }
    }

    public abstract class Person
    {
        public string Name { get; }
        public int Age { get; }

        protected Person(string name, int age)
        {
            this.Age = age;
            this.Name = name;
        }
    }

    public class Student : Person
    {
        public readonly Guid StuNumber;

        public Student(string name, int age) : base(name, age)
        {
            this.StuNumber = Guid.NewGuid();
        }

        public override string ToString()
        {
            return $"{this.Name},{this.Age}";
        }
    }

    public class Employee : Person
    {
        public double Salary { get; set; }

        public Employee(string name, int age, double salary) : base(name, age)
        {
            this.Salary = salary;
        }

        public override string ToString()
        {
            return $"{this.Name}, {this.Salary}";
        }
    }
    public class Mathematics
    {
        public static List<int> GetPrimes(int start, int end)
        {
            List<int> list = new List<int>();
            for (int i = start; i <= end; i++)
            {
                if (CalculatePrime(i))
                    list.Add(i);
            }
            list.ForEach(Console.WriteLine);
            return list;
        }

        private static bool CalculatePrime(int value)
        {
            if (value <= 1) return false;
            var possibleFactors = Math.Sqrt(value);
            for (var factor = 2; factor <= possibleFactors; factor++)
            {
                if (value % factor == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }


}
