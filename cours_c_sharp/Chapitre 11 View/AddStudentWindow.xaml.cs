﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Chapitre_11_Model;
using Chapitre_11_BLL;

namespace Chapitre_11_View
{
    /// <summary>
    /// Interaction logic for AddStudentWindow.xaml
    /// </summary>
    public partial class AddStudentWindow : Window
    {
        public AddStudentWindow()
        {
            InitializeComponent();
        }

        private static bool IsNameValid(string name)
        {
            return Regex.Match(name, @"^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$").Success;
        }

        private static bool IsYearsOfExperienceValid(string number)
        {
            return Regex.Match(number, @"^([0-9]|[0-9][0-9])$").Success;
        }

        private static bool ConvertCheckBox(CheckBox box)
        {
            return box.IsChecked == true ? true : false;
        }

        private static int? ConvertTextToNullableInt(TextBox box)
        {
            if (String.IsNullOrEmpty(box.Text))
            {
                return null;
            }
            else
            {
                return int.Parse(box.Text);
            }
        }

        private bool AreFieldOk()
        {
            if (!IsNameValid(LastNameTextBox.Text + " " + FirstNameTextBox.Text))
            {
                MessageBox.Show(this, "Name isn't valid.", "Error", MessageBoxButton.OK);
                return false;
            }
            if (!IsYearsOfExperienceValid(YoETextBox.Text) && !String.IsNullOrEmpty(this.YoETextBox.Text) && int.Parse(YoETextBox.Text) < 16)
            {
                MessageBox.Show(this, "Year of Experience isn't valid", "Error", MessageBoxButton.OK);
                return false;
            }

            return true;
        }

        private void FinalizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (AreFieldOk())
            {
                Student student = new Student(this.FirstNameTextBox.Text, this.LastNameTextBox.Text, ConvertCheckBox(this.isInternationalCheckBox),
                 ConvertTextToNullableInt(this.YoETextBox), this.BirthdatePicker.SelectedDate);

                Manager.AddStudent(student);
                this.Close();
                MessageBox.Show(this, $"Student with Id : {student.Id}\n" + "has been sucessfully added to the database!", "Succes", MessageBoxButton.OK);
            }

        }
    }
}