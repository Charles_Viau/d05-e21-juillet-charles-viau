﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Student
    {
        public string Name { get; }
        public int Age { get; }
        public DateTime? Birthday { get; }
        public bool? IsInternational { get; }
        public const int minimalAge = 16;

        public Student (string name, int age, DateTime? birthday, bool? isInternational)
        {
            this.Name = name;
            this.Age = age;
            this.Birthday = birthday;
            this.IsInternational = isInternational;
        }
    }
}
