﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public static class DBUtility
    {
        public static void HandleNull(string param, object obj, SqlCommand cmd)
        {
            if (obj == null)
            {
                cmd.Parameters.AddWithValue(param, DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue(param, obj);
            }
        }
    }
}
